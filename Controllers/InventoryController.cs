﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ShopBridgeAPI.Interfaces;
using ShopBridgeAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopBridgeAPI.Controllers
{
    [Route("Inventory")]
    [Authorize]
    public class InventoryController : ControllerBase
    {
        private readonly IInventoryManager _InventoryManager;
        private readonly ILogger<InventoryController> _logger;

        public InventoryController(IInventoryManager InventoryManager, ILogger<InventoryController> logger)
        {
            _InventoryManager = InventoryManager;
            _logger = logger;
        }
        [HttpGet]
        public async Task<IActionResult> GetInventorys()
        {
            try
            {
                _logger.LogInformation("Fetching list of all Inventorys from GetInventorys");
                return Ok(await _InventoryManager.GetInventoryItems());
            }
            catch (Exception ex)
            {
                _logger.LogError("GetInventorys failed: " + ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetInventoryById(int id)
        {
            try
            {
                _logger.LogInformation("Fetching Inventory with Id: " + id);
                return Ok(await _InventoryManager.GetInventoryItemById(id));
            }
            catch (Exception ex)
            {
                _logger.LogError("GetInventoryById failed: " + ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
        [HttpPost]
        public async Task<IActionResult> AddInventory([FromBody] Inventory Inventory)
        {
            try
            {
                _logger.LogInformation("Add a new Inventory");
                if (await _InventoryManager.AddInventoryItem(Inventory))
                    return Ok("Inventory added successfully");
                _logger.LogError("AddInventory failed!");
                return StatusCode(StatusCodes.Status500InternalServerError, "Failed to add the Inventory to the database");
            }
            catch (Exception ex)
            {
                _logger.LogError("AddInventory failed: " + ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> UpdateInventory(int id, [FromBody] Inventory Inventory)
        {
            try
            {
                _logger.LogInformation("Update Inventory");
                Inventory.ItemId = id;
                if (await _InventoryManager.UpdateInventoryItem(Inventory))
                    return Ok("Inventory updated successfully");
                _logger.LogError("UpdateInventory failed!");
                return StatusCode(StatusCodes.Status500InternalServerError, "Failed to update the Inventory to the database");
            }
            catch (Exception ex)
            {
                _logger.LogError("UpdateInventory failed: " + ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> DeleteInventory(int id)
        {
            try
            {
                _logger.LogInformation("Delete Inventory");
                if (await _InventoryManager.DeleteInventoryItem(id))
                    return Ok("Inventory deleted successfully");
                _logger.LogError("DeleteInventory failed!");
                return StatusCode(StatusCodes.Status500InternalServerError, "Failed to delete the Inventory to the database");
            }
            catch (Exception ex)
            {
                _logger.LogError("DeleteInventory failed: " + ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}

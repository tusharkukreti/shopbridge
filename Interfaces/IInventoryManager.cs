﻿using ShopBridgeAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopBridgeAPI.Interfaces
{
    public interface IInventoryManager
    {
        Task<List<Inventory>> GetInventoryItems();
        Task<Inventory> GetInventoryItemById(int id);
        Task<bool> AddInventoryItem(Inventory student);
        Task<bool> UpdateInventoryItem(Inventory student);
        Task<bool> DeleteInventoryItem(int id);
    }
}

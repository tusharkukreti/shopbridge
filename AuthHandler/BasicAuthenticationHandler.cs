﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace ShopBridgeAPI.AuthHandler
{

    public class BasicAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        //used a constant securty header value. This can be stored in user secrets or database.
        //we can also give username & password as a encrypted header value
        private const string SecurityHeader = "abcdef123";

        public BasicAuthenticationHandler(IOptionsMonitor<AuthenticationSchemeOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock)
            : base(options, logger, encoder, clock)
        {

        }
        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            try
            {
                if (!Request.Headers.ContainsKey("Authorization"))
                    return AuthenticateResult.Fail("Auth header missing");
                var AuthHeaderValue = AuthenticationHeaderValue.Parse(Request.Headers["Authorization"]);
                string headerValue = AuthHeaderValue.Parameter;

                //If header is encrypted, it can be decrypted here.

                if (headerValue == SecurityHeader)
                {
                    //Authorized
                    //claims can be used to fetch the user idenity
                    var claim = new[] { new Claim(ClaimTypes.Name, headerValue) };
                    var identity = new ClaimsIdentity(claim, Scheme.Name);
                    var principal = new ClaimsPrincipal(identity);
                    var ticket = new AuthenticationTicket(principal, Scheme.Name);

                    return AuthenticateResult.Success(ticket);
                }
                else
                {
                    //means the header is incorrect                    
                    return AuthenticateResult.Fail("Invalid Authorization value");
                }
            }
            catch (Exception ex)
            {
                return AuthenticateResult.Fail("Auth Failed due to : " + ex.Message);
            }
        }

    }
}

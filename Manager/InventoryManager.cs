﻿using ShopBridgeAPI.DatabaseContext;
using ShopBridgeAPI.Interfaces;
using ShopBridgeAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopBridgeAPI.Manager
{
    public class InventoryManager : IInventoryManager
    {
        private readonly Context _context;
        public InventoryManager(Context context)
        {
            _context = context;
        }
        public async Task<bool> AddInventoryItem(Inventory Inventory)
        {
            try
            {
                _context.Inventory.Add(Inventory);
                return await _context.SaveChangesAsync() == 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
              await  _context.DisposeAsync();
            }
        }

        public async Task<bool> DeleteInventoryItem(int id)
        {
            try
            {
                Inventory std = await _context.Inventory.FirstOrDefaultAsync(x => x.ItemId == id);
                if (std == null)
                    throw new Exception("Inventory Item not found!");
                _context.Inventory.Remove(std);
                return await _context.SaveChangesAsync() == 1;
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
              await  _context.DisposeAsync();
            }
        }

        public async Task<Inventory> GetInventoryItemById(int id)
        {
            try
            {
                Inventory std = await _context.Inventory.FirstOrDefaultAsync(x => x.ItemId == id);
                if (std == null)
                    throw new Exception("Inventory Item not found!");
                return std;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                await _context.DisposeAsync();
            }
        }

        public async Task<List<Inventory>> GetInventoryItems()
        {
            try
            {
                return await _context.Inventory.ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                await _context.DisposeAsync();
            }
        }

        public async Task<bool> UpdateInventoryItem(Inventory Inventory)
        {
            try
            {
                Inventory std = await _context.Inventory.FirstOrDefaultAsync(x => x.ItemId == Inventory.ItemId);
                if (std == null)
                    throw new Exception("Inventory Item not found!");
                std.Name = Inventory.Name;
                std.Description = Inventory.Description;
                std.Price = Inventory.Price;
                return await _context.SaveChangesAsync() == 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                await _context.DisposeAsync();
            }
        }
    }
}
